#!/usr/bin/env ts-node

const yargs = require('yargs');
import {CliNavigation} from "./navigation/cli.navigation";
const args = yargs.argv;
Object.keys(args).map((key: string) => {
  if(key.includes('-')) {
    delete args[key];
  }
});

new CliNavigation(args);
