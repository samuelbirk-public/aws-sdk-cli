import {SimpleCli} from './simple.cli';

export class S3Cli extends SimpleCli{
  args: any;
  command: any;
  cache: any = {};
  constructor(args: any, command: string = 's3') {
    super(args, command);
    this.service = 'S3';
  }
}