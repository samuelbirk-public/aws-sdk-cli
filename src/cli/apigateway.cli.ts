import {SimpleCli} from './simple.cli';

export class ApiGatewayCli extends SimpleCli{
  args: any;
  command: any;
  cache: any = {};
  constructor(args: any, command: string = 'api-gateway') {
    super(args, command);
    this.service = 'ApiGatewayV2';
  }
}