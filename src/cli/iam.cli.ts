import {SimpleCli} from './simple.cli';

export class IamCli extends SimpleCli{
  args: any;
  command: any;
  cache: any = {};
  constructor(args: any, command: string = 'iam') {
    super(args, command);
    this.service = 'IAM';
  }
}