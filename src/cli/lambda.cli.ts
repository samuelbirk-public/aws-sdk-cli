import {SimpleCli} from './simple.cli';

export class LambdaCli extends SimpleCli{
  args: any;
  command: any;
  cache: any = {};
  constructor(args: any, command: string = 'lambda') {
    super(args, command);
    this.service = 'Lambda';
  }
}