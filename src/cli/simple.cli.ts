import * as AWS from 'aws-sdk';
import {AwsCredentials} from "../credentials/aws.credentials";
import {LoggerService} from "../services/logger.service";
import {AwsSdkService} from '../services/aws-sdk.service';

export class SimpleCli {
  args: any;
  command: any;
  cache: any = {};
  service: string = '';
  runCommand: string = '';
  region: string = '';
  debug: boolean = false;
  constructor(args: any, command: string = 'simple-cli') {
    this.args = args;
    this.debug = this.args.debug || false;
    this.region = this.args.region || 'us-east-1';

    delete this.args.debug;
    delete this.args.region;
    delete this.args.verbose;
  }

  async beforeRun() {
    return;
  }

  async run() {
    if([undefined, null, ''].includes(this.service)) {
      LoggerService.error('You must provide a valid aws service');
    }
    if([undefined, null, ''].includes(this.args.run)) {
      AwsSdkService.printMethods(this.service);
      return
    }


    await this.beforeRun();
    if(this.debug) {
      LoggerService.debug(this.args);
    }
    this.runCommand = this.args.run;
    delete this.args.run;
    try {
      const params: any = {};
      //Object.keys(this.args).map((key: string) => params[this.capitalize(key)] = this.args[key]);

      const result = await this.request(this.runCommand, this.args);
      if(this.debug) {
        LoggerService.debug(result);
      }
      return result;
    } catch(result) {
      console.log(result)
      LoggerService.error(result.error);
      return null;
    }

  }

  capitalize(s: string) {
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  sdk(): any {
    const credentials = this.credentials();
    console.log(credentials);
    return this.cache.sdk = this.cache.sdk || new (AWS as any)[this.service]({credentials: credentials, region: this.region});
  }

  request(method: string, params: any = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      (this.sdk() as any)[method](params, (err: any, data: any) => {
        if(err) {
          LoggerService.warn(`${this.runCommand}::${JSON.stringify(params)} ${err.message}`)
          //console.log(err)
          reject({error: `${this.runCommand}::${JSON.stringify(params)} ${err.message}`, fullError: err});
        } else {
          resolve( data);
        }
      })
    })
  }

  credentials() {
    return AwsCredentials.get({...this.args, region: this.region}, this.command);
  }
}