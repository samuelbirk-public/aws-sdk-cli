import {SimpleCli} from './simple.cli';

export class SqsCli extends SimpleCli{
  args: any;
  command: any;
  cache: any = {};
  constructor(args: any, command: string = 'sqs') {
    super(args, command);
    this.service = 'SQS';
  }
}