import {SimpleCli} from './simple.cli';
import {ShellService} from '../services/shell.service';
import {question} from 'readline-sync';

export class EcrCli extends SimpleCli{
  args: any;
  command: any;
  cache: any = {};
  constructor(args: any, command: string = 'lambda') {
    super(args, command);
    this.service = 'ECR';
  }

  async beforeRun() {
    await new Promise((resolve, reject) => {
      let accountId;
      if(this.args.accountId !== undefined) {
        accountId =this.args.accountId;
      } else if(process.env.AWS_ACCOUNT_ID !== undefined) {
        accountId = process.env.AWS_ACCOUNT_ID;
      } else {
        accountId = question(`Please enter your account id you are trying to access: `);
      }
      const result = ShellService.command(`docker login --username AWS --password "${this.password().trim()}" ${accountId}.dkr.ecr.us-east-1.amazonaws.com`, this.debug);
      resolve(result);
    })
  }


  password() {
    return this.cache.dockerPassword = this.cache.dockerPassword || ShellService.command(`aws ecr get-login-password`);
  }
}