import {SimpleCli} from './simple.cli';

export class DynamoDBCli extends SimpleCli{
  args: any;
  command: any;
  cache: any = {};
  constructor(args: any, command: string = 'dynamodb') {
    super(args, command);
    this.service = 'DynamoDB';
  }
}