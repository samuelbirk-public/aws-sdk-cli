import * as AWS from "aws-sdk";

export class AwsCredentials {
  args: any;
  command: string;
  resource: string;
  accountId: string;
  iamRoleName: string;
  cacheFile: string;
  constructor(args: any, command: string = 'credentials') {
    this.args = args;
    this.resource = this.args.resource || this.args.typeName;
    this.command = command;
    this.accountId  = this.args.accountId || process.env.CFN_ISENGARD_ACCOUNT_ID;
    this.iamRoleName = this.args.iamRoleName || process.env.CFN_ISENGARD_IAM_ROLE_NAME;
    this.cacheFile = `credentials-${this.accountId}-${this.iamRoleName}`;
  }

  static get(args: any = {}, command = '') {
    const credentials: any = new AwsCredentials(args, command).run();
    console.log(credentials)
    return new AWS.Credentials(credentials);
  }

  run() {
    if(process.env.AWS_SESSION_TOKEN) {
      return {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        sessionToken: process.env.AWS_SESSION_TOKEN
      };
    }
    return {
       accessKeyId: process.env.AWS_ACCESS_KEY_ID,
       secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    };
  }
}