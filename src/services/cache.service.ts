import {appendFileSync, writeFileSync} from "fs";
import {ShellService} from "./shell.service";
import {LoggerService} from "./logger.service";
import {FileService} from "./file.service";
import {basename} from 'path';

export class CacheService {
  static cacheExists(type: string) {
    return ShellService.fileExists(`${this.directory()}/${type}.json`);
  }

  static directory() {
    return `${__dirname.split('aws-sdk-cli')[0]}/aws-sdk-cli/cache`
  }

  static fullDirectory(fileName: string) {
    const pathParts = fileName.split('/');
    pathParts.pop();
    const path = pathParts.join('/')
    return `${this.directory()}/${path}`.replace(/\/\//g, '/').replace(/\/$/, '');
  }


  static fullPath(fileName: string) {
    const finalName = basename(fileName).replace(/\.json$/, '')
    const finalDirectory = this.fullDirectory(fileName);
    return `${finalDirectory}/${finalName}.json`;
  }

  static writeCacheFile(fileName: string, obj: any, debug: boolean = false) {
    const finalName = basename(fileName).replace(/\.json$/, '')
    const finalDirectory = this.fullDirectory(fileName);
    if(!ShellService.dirExists(`${finalDirectory}`)){
      ShellService.command(`mkdir -p ${finalDirectory}`)
    }
    if(!ShellService.fileExists(`${finalDirectory}/${finalName}.json`)){
      ShellService.command(`touch ${finalDirectory}/${finalName}.json`)
    }
    writeFileSync(`${finalDirectory}/${finalName}.json`, JSON.stringify(obj, null, 2), 'utf8');
    if(debug) {
      LoggerService.grey(`Wrote output to: ${this.directory()}/${finalName}.json`)
    }
    return `${finalDirectory}/${finalName}.json`;
  }

  static appendCacheFile(fileName: string, obj: any, debug: boolean = false) {
    if(obj.length === 0) {
      return
    }
    const finalName = basename(fileName).replace(/\.json$/, '')
    const finalDirectory = this.fullDirectory(fileName);
    if(!ShellService.dirExists(`${finalDirectory}`)){
      ShellService.command(`mkdir -p ${finalDirectory}`)
    }
    if(!ShellService.fileExists(`${finalDirectory}/${finalName}.json`)){
      ShellService.command(`touch ${finalDirectory}/${finalName}.json`)
    }

    let content =  JSON.stringify(obj, null, 2).trim().replace(/^\[/, '').replace(/\]$/, ',');
    appendFileSync(`${finalDirectory}/${finalName}.json`, content, 'utf8');
    if(debug) {
      LoggerService.grey(`Wrote output to: ${this.directory()}/${finalName}.json`)
    }
    return `${finalDirectory}/${finalName}.json`;
  }

  static readCacheFile(fileName: string) {
    const finalName = basename(fileName).replace(/\.json$/, '')
    const finalDirectory = this.fullDirectory(fileName);

    ShellService.command(`mkdir -p ${finalDirectory}`)
    if(!ShellService.fileExists(`${finalDirectory}/${finalName}.json`)){
      return null;
    }
    return FileService.read(`${finalDirectory}/${finalName}.json`)
  }

  static destroy(fileName: string) {
    const finalName = basename(fileName).replace(/\.json$/, '')
    const finalDirectory = this.fullDirectory(fileName);

    return ShellService.command(`rm -rf ${finalDirectory}/${finalName}.json`, true)
  }
}
