import {readdirSync} from "fs";
import {pwd} from 'shelljs';
import {ShellService} from "./shell.service";

export class PackageService {

  static root(): any {
    const rootDirectory = this.findUp('package.json');
    if(rootDirectory === '') {
      //LoggerService.warn('CANNOT FIND PACKAGE ROOT DIRECTORY. NO package.json FILE FOUND.');
      const currentDirectory = ShellService.command('pwd');
      //LoggerService.info(`USING CURRENT DIRECTORY INSTEAD ${currentDirectory}`);
      return currentDirectory.trim();
    } else {
      return rootDirectory;
    }
  }

  static isNodeDirectory() {
    return this.findUp('package.json') !== ''
  }

  static packageName() {
    if(!this.isNodeDirectory()) {
      return ''
    }
    return this.file().name;
  }

  static description() {
    if(!this.isNodeDirectory()) {
      return ''
    }
    return this.file().description;
  }

  static version(packageName: string = '') {
    return this.lockFile().packages[`node_modules/${packageName}`].version;
  }

  static file() {
    if(!this.isNodeDirectory()) {
      return ''
    }
    return require(`${this.root()}/package.json`);
  }

  static lockFile() {
    if(!this.isNodeDirectory()) {
      return ''
    }
    return require(`${this.root()}/package-lock.json`);
  }

  static findUp(fileName: string, directory: string|null = null): string {
    const currentDirectory = pwd().toString();
    directory = directory || currentDirectory;
    directory = directory.trim();
    const files: string[] = readdirSync(directory);

    if(files.includes(fileName)) {
      return directory;
    } else {
      const pathParts = directory.split('/');
      pathParts.pop();
      const up1_path: string = pathParts.join('/');
      if(up1_path.trim().replace(/\//g, '') === '') {
        return ''
      } else {
        return this.findUp(fileName, up1_path);
      }
    }
  }
}