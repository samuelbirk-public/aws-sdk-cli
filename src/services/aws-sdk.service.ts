import {resolve} from "path";

import * as TJS from "typescript-json-schema";
import {LoggerService} from "./logger.service";
import {uniq} from 'underscore';
import * as AWS from "aws-sdk";

export class AwsSdkService {
  args: any;
  command: string;
  typeName: string = '';
  resource: string = '';
  service: string = '';
  cache: any = {};
  constructor(args: any, command: string = 'typescript-schema') {
    this.args = args;
    this.command = command;
    this.service = this.args.service;
  }

  run(): any {
    if(this.args.method) {
      console.log(this.simpleSearchMethods(this.args.method));
    } else if(this.args.methods) {
      const aws: any = AWS;
      console.log(this.service);
      const serviceInstance: any = new aws[this.service]({region: 'us-east-1'});
      LoggerService.cyan(this.getMethods(serviceInstance))
    }
  }

  static printMethods(service: string) {
    new AwsSdkService({service: service, methods: true}).run()
  }

  simpleSearchMethods(searchTerm: string = '') {
    const aws: any = AWS;
    const serviceInstance: any = new aws[this.service]({region: 'us-east-1'});
    const results = this.getMethods(serviceInstance).filter((methodName: string) => {
      return methodName.toLowerCase().includes(searchTerm.toLowerCase())
    });
    return results;
  }

  getMethods (obj: any) {
    let properties: any[] = [];
    let currentObj = obj;
    do {
      Object.getOwnPropertyNames(currentObj).map(item => properties.push(item))
    } while ((currentObj = Object.getPrototypeOf(currentObj)));
    return uniq([...properties]).filter(item => typeof obj[item] === 'function').filter((name) => {
      const nonDistinctMethods = [
        'MONITOR_EVENTS_BUBBLE',
        'CALL_EVENTS_BUBBLE',
        'listeners',
        'on',
        'constructor',
        'onAsync',
        'removeListener',
        'removeAllListeners',
        'emit',
        'callListeners',
        'addListeners',
        'addNamedListener',
        'addNamedAsyncListener',
        'addNamedListeners',
        'addListener',
        'hasOwnProperty',
        'isPrototypeOf',
        'propertyIsEnumerable',
        'toString',
        'valueOf',
        'toLocaleString'
      ]

      if(name.startsWith('__') || nonDistinctMethods.includes(name)) {
        return false;
      } else {
        return true;
      }
    });
  }

  directory() {
    return `${__dirname.split('aws-sdk-cli')[0]}/aws-sdk-cli/node_modules/aws-sdk/clients`
  }

  filePath(): string {
    return `${this.directory()}/${this.service.toLowerCase()}.d.ts`;
  }

  generator(): any {
    const program = TJS.getProgramFromFiles(
        [resolve(this.filePath())],
    );
    const thing =  TJS.buildGenerator(program, {required: true});
    return this.cache.generator = this.cache.generator ||  TJS.buildGenerator(program, {required: true});
  }

}