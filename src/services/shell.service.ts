import {LoggerService} from "../services/logger.service";
import {exec, test} from "shelljs";
import {spawn} from "child_process";
import {isEmpty} from 'underscore';

export class ShellService {
  static command(command: string, debug = false) {
    if(debug) {
      LoggerService.grey(`Running: $ ${command}`);
    }
    const res: any = exec(command, {silent: !debug});
    if (res.code !== 0) {
      LoggerService.error(res.stderr);
      return {error: res.stderr}
      //exit(1)
    } else {
      return res.stdout
    }
  }

  static kill(search: string, debug = false) {
    const process = this.command(`ps aux | grep '${search}'`)
    const processes = process ? process.trim().split(/\n/) : [];
    if(debug) {
      LoggerService.grey(`Processes related to ${search}`)
      LoggerService.debug(processes)
    }
    if(isEmpty(processes)) { return; }
    const pidStrings = processes.filter((process : string) => !process.includes('grep'));
    if(isEmpty(pidStrings)) { return }
    if([undefined, null, ''].includes(pidStrings[0])) { return }
    const pid = pidStrings[0].split(/\s+/)[1];
    if(debug) {
      LoggerService.grey(`Killing: ${pid}`)
    }
    if(![undefined, null, ''].includes(pid)) {
      if(debug) {
        LoggerService.grey(`Killing: ${process}`)
      }
      this.command(`kill -9 ${pid}`, debug);
    }
  }

  static backgroundCommand(command: string, debug: boolean = false) {
    if(debug) {
      LoggerService.grey(`Running in background: $ ${command}`)
    }

    const args = command.split(' ');
    const cmd: string = args.shift() || '';
    spawn(cmd, args, {stdio: 'ignore', detached: true }).unref();
  }


  static fileExists(path: string) {
    return test('-f', path);
  }

  static dirExists(path: string) {
    return test('-d', path);
  }

}
