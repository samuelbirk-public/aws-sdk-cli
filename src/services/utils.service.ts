import {decapitalize} from 'underscore.string';

export class UtilsService {
  static deepClone(obj: any) {
    return JSON.parse(JSON.stringify(obj));
  }

  static capitalize(str: string = '') {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  static uncapitalize(str: string = '') {
    return str.charAt(0).toLowerCase() + str.slice(1);
  }

  static recursiveSearch(obj: any, searchKey: string, results: any[] = []) {
    const r = results;
    Object.keys(obj).forEach(key => {
      const value = obj[key];
      if(key === searchKey && typeof value !== 'object'){
        r.push(value);
      }else if(typeof value === 'object'){
        this.recursiveSearch(value, searchKey, r);
      }
    });
    return r;
  };

  static camelToUnderscore(text: string = '') {
    return text.replace(/(.)([A-Z][a-z]+)/, '$1_$2')
        .replace(/([a-z0-9])([A-Z])/, '$1_$2')
        .replace(/([a-z0-9])([A-Z])/, '$1_$2')
        .replace(/([a-z0-9])([A-Z])/, '$1_$2')
        .replace(/([a-z0-9])([A-Z])/, '$1_$2')
        .toLowerCase()
  }

  static snakeToTitleCase(text: string = '') {
    return text.toLowerCase().replace(/(?:^|[\s-/])\w/g, function (match) {
      return match.toUpperCase();
    }).replace(/\-/g, '');
  }

  static camelToSnakeCase(text: string = '') {
    return text
        .replace(/(.)([A-Z][a-z]+)/, '$1-$2')
        .replace(/([a-z0-9])([A-Z])/, '$1-$2')
        .replace(/([a-z0-9])([A-Z])/, '$1-$2')
        .replace(/([a-z0-9])([A-Z])/, '$1-$2')
        .replace(/([a-z0-9])([A-Z])/, '$1-$2')
        .replace(/([a-z0-9])([A-Z])/, '$1-$2')
        .toLowerCase()
  }

  static titleToCamelCase(text: string = '') {
    return decapitalize(this.snakeToTitleCase(this.camelToSnakeCase(text)))
  }

}
