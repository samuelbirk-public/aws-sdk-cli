import {readdirSync, readFileSync, statSync, writeFileSync} from "fs";
import {ShellService} from './shell.service';
import * as jsYaml from "js-yaml";
import {PackageService} from "./package.service";
import {LoggerService} from "./logger.service";

export class FileService {

  static exists(path: string) {
    return ShellService.fileExists(this.fullPath(path))
  }

  static writeJson(path: string, obj: any) {
    this.write(path, JSON.stringify(obj, null, 2));
  }

  static writeJsonUnwrapped(path: string, obj: any) {
    let final: any = '';
    if(Array.isArray(obj)) {
      final = obj.map((item) => {
         return JSON.stringify(item)
      }).join('\r\n')
    }
    this.write(path, final);
  }

  static readJson(path: string) {
    let json: any;
    try {
      json = this.read(path);
    } catch(e) {
      LoggerService.warn(`No file at ${path}`);
      json = '{}'
    }
    return JSON.parse(json);
  }


  static readJsonWrapped(path: string) {
    let json: any;
    try {
      json = this.read(path);
    } catch(e) {
      LoggerService.warn(`No file at ${path}`);
      json = '{}'
    }
    return JSON.parse(`[${json.replace(/[\r\n]/gm, ',')}]`.replace(/,\]$/, ']'));
  }


  static readYaml(path: string) {
    const yaml = this.read(path);
    return jsYaml.load(yaml);
  }

  static writeYaml(path: string, obj: any) {
    const yaml = jsYaml.dump(obj);
    this.write(path, yaml)
  }

  static write(path: string, contents: any) {
    const fullPath = this.fullPath(path);
    const splitPath = fullPath.split('/');
    splitPath.pop();
    const dirPath = splitPath.join('/');
    ShellService.command(`mkdir -p ${dirPath}`);
    writeFileSync(this.fullPath(path), contents, 'utf8');
  }

  static remove(path: string) {
    const fullPath = this.fullPath(path);
    ShellService.command(`rm -rf ${fullPath}`);
  }

  static removeDir(path: string) {
    this.remove(path);
  }

  static read(path: string) {
    return readFileSync(this.fullPath(path), 'utf8');
  }

  static lastModified(path: string) {
    return statSync(this.fullPath(path)).mtime;
  }

  static createdAt(path: string) {
    return statSync(this.fullPath(path)).ctime;
  }

  static directories(path: string) {
    console.log(this.fullPath(path))
    return readdirSync(this.fullPath(path));
  }

  static fullPath(path: string) {
    if(path.startsWith('/')) {
      return path;
    } else {
      return `${PackageService.root()}/${path.replace(/^\.\//, '')}`
    }
  }
}