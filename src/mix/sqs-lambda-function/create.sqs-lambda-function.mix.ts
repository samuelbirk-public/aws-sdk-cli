import {EcrCli} from '../../cli/ecr.cli';
import {ShellService} from '../../services/shell.service';
import {question} from 'readline-sync';
import {UtilsService} from '../../services/utils.service';
import {LambdaCli} from '../../cli/lambda.cli';
import {LoggerService} from '../../services/logger.service';
import {UUID} from 'angular2-uuid';
import {SqsCli} from "../../cli/sqs.cli";
import {IamCli} from "../../cli/iam.cli";

export class CreateSqsLambdaFunctionMix {
  args: any;
  command: any;
  cache: any;
  step: any;
  constructor(args: any, command: string = 'lambda-mix') {
    this.args = args;
    this.cache = this.args.cache || {};
    this.step = this.args.step || 1;
  }

  async run() {
    if(this.args.method === undefined || this.args.all) {
      this.args.methods = [
        'ensureDockerRepo',
        'buildAndPushDockerImage',
        'createSqsQueue',
        'createLambdaRole',
        'createLambdaFunction',
        'createTrigger',
      ];
    }

    this.args.method = this.args.methods.shift();
    if(this.args.method !== undefined) {
      const result = await this.safeRun(this.args.method);
      if(result === 'error') {
        return;
      }
      await new CreateSqsLambdaFunctionMix({...this.args, step: this.step, cache: this.cache}).run();
    } else {
      if(this.args.debug) {
        LoggerService.debug(this.cache);
      }
      return this.cache;
    }
  }

  async safeRun(method: string) {
    this.step = LoggerService.step(this.step, `Running $ ${method}`);
    try {
      return (this as any)[method]();
    } catch (error) {
      LoggerService.error(error);
      return 'error'
    }
  }


  async getSqsQueueUrl() {
    return this.cache.getSqsQueueUrl = this.cache.getSqsQueueUrl  || await new SqsCli({
      run: 'getQueueUrl', QueueName: `${this.args.queueName}.fifo`,
      debug: this.args.debug,
    }).run()
  }

  async getSqsQueue() {
    const sqsQueueUrL: any = await this.getSqsQueueUrl()

    return this.cache.getSqsQueue = this.cache.getSqsQueue  || await new SqsCli({
      run: 'getQueueAttributes', QueueUrl: sqsQueueUrL.QueueUrl, AttributeNames: ['All'],
      debug: this.args.debug,
    }).run()
  }

  async createSqsQueue() {

    return this.cache.createSqsQueue = this.cache.createSqsQueue  || await new SqsCli({
      run: 'createQueue', QueueName: `${this.args.queueName}.fifo`, Attributes: {
        "FifoQueue": "true"
      },
      debug: this.args.debug,
    }).run()
  }

  async createTrigger() {
    const lambda = await this.getLambdaFunction();
    const sqsQueue: any = await this.getSqsQueue()
    console.log('lambda', lambda)
    console.log('sqsQueue', sqsQueue.Attributes.QueueArn)
    this.cache.createTrigger = await new LambdaCli({
      run: 'createEventSourceMapping',
      debug: this.args.debug,
      FunctionName: lambda.Configuration.FunctionName,
      EventSourceArn: sqsQueue.Attributes.QueueArn,
      BatchSize: 5,
    }).run();
  }

  async getLambdaFunction() {
    return this.cache.getLambdaFunction = this.cache.getLambdaFunction  || await new LambdaCli({
      run: 'getFunction', FunctionName: this.findValue('RepositoryName'),
      debug: this.args.debug,
    }).run()
  }

  sleep(seconds = 0) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < (seconds * 1000));
  }

  async getLambdaRole() {
    return this.cache.getLambdaRole = this.cache.getLambdaRole  || await new IamCli({
      run: 'getRole',  RoleName: `${this.findValue('RepositoryName')}-lambda-role` ,
      debug: this.args.debug,
    }).run()
  }

  async createLambdaRole() {
    if(this.cache.createLambdaRole !== undefined) {
      return await this.getLambdaRole();
    }
    await new IamCli({
      run: 'createRole',
      debug: this.args.debug,
      RoleName: `${this.findValue('RepositoryName')}-lambda-role`,
      AssumeRolePolicyDocument: JSON.stringify({
        "Version": "2012-10-17",
        "Statement": [
          {
            "Effect": "Allow",
            "Principal": {
              "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
          }
        ]
      }),
    }).run();
    const role = await this.getLambdaRole()
    console.log('role::::', role)
    await this.createLambdaPolicy()
    return this.cache.createLambdaRole = await this.getLambdaRole();

  }

  async getLambdaPolicy() {
    return this.cache.getLambdaRole = this.cache.getLambdaRole  || await new IamCli({
      run: 'getPolicy', PolicyName: `${this.findValue('RepositoryName')}-lambda-policy` ,
      debug: this.args.debug,
    }).run()
  }

  async createLambdaPolicy() {
    if(this.cache.createLambdaPolicy !== undefined) {
      return await this.getLambdaPolicy();
    }

    await new IamCli({
      run: 'putRolePolicy',
      debug: this.args.debug,
      RoleName: `${this.findValue('RepositoryName')}-lambda-role`,
      PolicyName: `${this.findValue('RepositoryName')}-lambda-policy`,
      PolicyDocument: JSON.stringify({
        "Version": "2012-10-17",
        "Statement": [
          {
            "Effect": "Allow",
            "Action": "lambda:InvokeFunction",
            "Resource": `arn:aws:lambda:${this.findValue('AwsRegion')}:${this.findValue('AwsAccountId')}:function:${this.findValue('RepositoryName')}*`
          },
          {
            "Effect": "Allow",
            "Action": [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:PutLogEvents"
            ],
            "Resource": `arn:aws:logs:${this.findValue('AwsRegion')}:${this.findValue('AwsAccountId')}:*`
          },
          {
            "Effect": "Allow",
            "Action": [
              "sqs:ReceiveMessage",
              "sqs:DeleteMessage",
              "sqs:GetQueueAttributes",
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:PutLogEvents"
            ],
            "Resource": `*`
          },
          {
            "Effect": "Allow",
            "Action": [
              "redshift-data:*",
              "redshift:*"
            ],
            "Resource": `*`
          },
        ]
      }),

    }).run();
    return this.cache.createLambdaPolicy = await this.getLambdaPolicy();
  }

  async createLambdaFunction(attempts = 0) {
    if(this.cache.createLambdaFunction !== undefined) {
      const lambdaFunction = await this.getLambdaFunction();

      if(attempts > 10) {
        LoggerService.error('Failed to get APIGateway')
      }
      else if(lambdaFunction !== undefined) {
        return this.cache.createLambdaFunction = lambdaFunction;
      } else {
        const nextAttempt = attempts++;
        LoggerService.info(`Trying again in ${nextAttempt} seconds.`);
        this.sleep(nextAttempt);
        await this.createLambdaFunction(nextAttempt);
      }
    }
    const role: any = await this.getLambdaRole()
    console.log('ROLE ROLE', role)
    this.cache.createLambdaFunction = await new LambdaCli({
      run: 'createFunction',
      debug: this.args.debug,
      FunctionName: this.findValue('RepositoryName'),
      Role: role.Role.Arn,
      Code: {
        ImageUri:`${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`,
      },
      PackageType: 'Image'
    }).run();
    const lambdaFunction = await this.getLambdaFunction();
    if(lambdaFunction !== undefined) {
      return this.cache.createLambdaFunction ;
    } else {
      this.createLambdaFunction(attempts++)
    }
  }


  imageTag() {
    return this.args.tag || 'latest'
  }

  buildAndPushDockerImage() {
    ShellService.command(`docker build -t ${this.findValue('RepositoryName')} .`, this.args.debug);
    ShellService.command(`docker tag ${this.findValue('RepositoryName')}:${this.imageTag()} ${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`, this.args.debug);
    ShellService.command(`docker push ${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`, this.args.debug);
  }

  async ensureDockerRepo() {
    await new EcrCli({run: 'createRepository', debug: this.args.debug, repositoryName: this.findValue('RepositoryName')}).run();
  }

  findValue(variableName: string, defaultValue?: string) {
    if(this.cache.findValue === undefined) {
      this.cache.findValue = {}
    }
    if(this.cache.findValue[variableName] !== undefined) {
      return this.cache.findValue[variableName];
    }
    const camelCase = UtilsService.titleToCamelCase(variableName);
    const CONSTANT_CASE = UtilsService.camelToUnderscore(camelCase).toUpperCase();
    let value;
    if(this.args[camelCase] !== undefined) {
      value = this.args[camelCase];
    } else if(process.env[CONSTANT_CASE] !== undefined) {
      value = process.env[CONSTANT_CASE];
    } else {
      if(defaultValue === undefined) {
        value = question(`Please enter the value for ${variableName}: `);
      } else {
        value = question(`Please enter the value for ${variableName} [${defaultValue}]: `, {defaultInput: defaultValue});
      }
    }
    return this.cache.findValue[variableName] = value;
  }


}