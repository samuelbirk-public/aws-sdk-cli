import {question} from 'readline-sync';
import {UtilsService} from '../../services/utils.service';
import {LambdaCli} from '../../cli/lambda.cli';
import {LoggerService} from '../../services/logger.service';
import {ShellService} from "../../services/shell.service";
import {EcrCli} from "../../cli/ecr.cli";

export class UpdateSqsLambdaFunctionMix {
  args: any;
  command: any;
  cache: any;
  step: any;
  constructor(args: any, command: string = 'lambda-mix') {
    this.args = args;
    this.cache = this.args.cache || {};
    this.step = this.args.step || 1;
  }

  async run() {
    await new EcrCli(this.args, this.command).beforeRun();

    if(this.args.methods === undefined || this.args.all) {
      this.args.methods = [
        'buildAndPushDockerImage',
        'updateFunctionConfiguration',
        'updateFunctionCode',
      ];
    }

    const method = this.args.methods.shift();
    if(method !== undefined) {
      const result = await this.safeRun(method);
      if(result === 'error') {
        return;
      }
      await new UpdateSqsLambdaFunctionMix({...this.args, step: this.step, cache: this.cache}).run();
    } else {
      if(this.args.debug) {
        LoggerService.debug(this.cache);
      }
      return this.cache;
    }
  }


  async safeRun(method: string) {
    this.step = LoggerService.step(this.step, `Running $ ${method}`);
    try {
      return (this as any)[method]();
    } catch (error) {
      LoggerService.error(error);
      return 'error'
    }
  }

  imageTag() {
    return this.args.tag || 'latest'
  }

  async updateFunctionCode() {
    return this.cache.updateFunctionCode = this.cache.updateFunctionCode  || await new LambdaCli({
      run: 'updateFunctionCode',
      debug: this.args.debug,
      FunctionName: this.findValue('RepositoryName'),
      ImageUri:`${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`,
    }).run()
  }


  translatedEnvironmentVariables() {
    if([undefined, null, ''].includes(this.args.env) && [undefined, null, ''].includes(this.args.environment)) {
      return '';
    } else {
      const environmentMap: any = {};
      this.args.env.split(',').map((envVariable: string) => {
        const [variable, value] = envVariable.split('=');
        environmentMap[variable.trim()] = value.trim();
      });

      return environmentMap
    }
  }


  async updateFunctionConfiguration() {
    if([undefined, null, ''].includes(this.args.env) && [undefined, null, ''].includes(this.args.environment)) {
      return;
    }
    return this.cache.updateFunctionConfiguration = this.cache.updateFunctionConfiguration  || await new LambdaCli({
      run: 'updateFunctionConfiguration',
      debug: this.args.debug,
      FunctionName: this.findValue('RepositoryName'),
      Environment: {Variables: this.translatedEnvironmentVariables() }
    }).run()
  }

  buildAndPushDockerImage() {
    ShellService.command(`docker build -t ${this.findValue('RepositoryName')} .`, this.args.debug);
    ShellService.command(`docker tag ${this.findValue('RepositoryName')}:${this.imageTag()} ${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`, this.args.debug);
    const res = ShellService.command(`docker push ${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`, this.args.debug);
    if(res.error && res.error.includes('Reauthenticate')) {

    }
    console.log('res', res)
  }

  findValue(variableName: string, defaultValue?: string) {
    if(this.cache.findValue === undefined) {
      this.cache.findValue = {}
    }
    if(this.cache.findValue[variableName] !== undefined) {
      return this.cache.findValue[variableName];
    }
    const camelCase = UtilsService.titleToCamelCase(variableName);
    const CONSTANT_CASE = UtilsService.camelToUnderscore(camelCase).toUpperCase();
    console.log('variableName', variableName);
    console.log('camelCase', camelCase);
    console.log('CONSTANT_CASE', CONSTANT_CASE);
    let value;
    if(this.args[camelCase] !== undefined) {
      value = this.args[camelCase];
    } else if(process.env[CONSTANT_CASE] !== undefined) {
      value = process.env[CONSTANT_CASE];
    } else {
      if(defaultValue === undefined) {
        value = question(`Please enter the value for ${variableName}: `);
      } else {
        value = question(`Please enter the value for ${variableName} [${defaultValue}]: `, {defaultInput: defaultValue});
      }
    }
    return this.cache.findValue[variableName] = value;
  }
}