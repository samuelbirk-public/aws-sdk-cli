import {question} from 'readline-sync';
import {UtilsService} from '../../services/utils.service';
import {LambdaCli} from '../../cli/lambda.cli';
import {LoggerService} from '../../services/logger.service';
import {ShellService} from "../../services/shell.service";
import {S3Cli} from "../../cli/s3.cli";
import {exit} from "shelljs";

export class UpdateLambdaFunctionMix {
  args: any;
  command: any;
  cache: any;
  step: any;
  constructor(args: any, command: string = 'lambda-mix') {
    this.args = args;
    this.cache = this.args.cache || {};
    this.step = this.args.step || 1;
  }

  async run() {
    console.log(this.s3bucket())
    if(this.args.methods === undefined || this.args.all) {
      this.args.methods = [
        'clean',
        'ensureS3Bucket',
        'npmInstall',
        'zipFunctionCode',
        'updateFunctionCode',
      ];
    }

    const method = this.args.methods.shift();
    if(method !== undefined) {
      const result = await this.safeRun(method);
      if(result === 'error') {
        return;
      }
      await new UpdateLambdaFunctionMix({...this.args, step: this.step, cache: this.cache}).run();
    } else {
      if(this.args.debug) {
        LoggerService.debug(this.cache);
      }
      return this.cache;
    }
  }

  async ensureS3Bucket() {
    try {
      await new S3Cli(this.args, this.command).request('createBucket', {Bucket: this.s3bucket()})
    }
    catch(e) {
      if( e.fullError.code !== 'BucketAlreadyOwnedByYou') {
        exit(1)
      }
    }
  }
  async safeRun(method: string) {
    this.step = LoggerService.step(this.step, `Running $ ${method}`);
    try {
      return (this as any)[method]();
    } catch (error) {
      LoggerService.error(error);
      return 'error'
    }
  }

  clean() {
    //ShellService.command(`tsc -p .`, this.args.debug);
    ShellService.command(`rm -rf ./dist`, this.args.debug);
    ShellService.command('find . -name \*.zip -delete', this.args.debug);
  }

  zipFunctionCode() {
    //ShellService.command(`tsc -p .`, this.args.debug);
    ShellService.command(`zip -r -X ${this.zipPath()} .`, this.args.debug);
    ShellService.command(`aws s3 cp ${this.zipPath()} s3://${this.s3bucket()}`, this.args.debug);
  }

  npmInstall() {
    ShellService.command(`npm install`, this.args.debug);
  }

  s3bucket() {
    return UtilsService.camelToSnakeCase(`birksb6-${this.findValue('RepositoryName')}`).slice(0, 60);
  }

  zipPath() {
    return `${this.findValue('RepositoryName')}.zip`;
  }

  async updateFunctionCode() {
    return this.cache.updateFunctionCode = this.cache.updateFunctionCode  || await new LambdaCli({
      run: 'updateFunctionCode',
      debug: this.args.debug,
      region: this.args.region,
      FunctionName: this.findValue('RepositoryName'),
      S3Bucket: this.s3bucket(),
      S3Key: this.zipPath()
    }).run()
  }

  findValue(variableName: string, defaultValue?: string) {
    if(this.cache.findValue === undefined) {
      this.cache.findValue = {}
    }
    if(this.cache.findValue[variableName] !== undefined) {
      return this.cache.findValue[variableName];
    }
    const camelCase = UtilsService.titleToCamelCase(variableName);
    const CONSTANT_CASE = UtilsService.camelToUnderscore(camelCase).toUpperCase();
    console.log('variableName', variableName);
    console.log('camelCase', camelCase);
    console.log('CONSTANT_CASE', CONSTANT_CASE);
    let value;
    if(this.args[camelCase] !== undefined) {
      value = this.args[camelCase];
    } else if(process.env[CONSTANT_CASE] !== undefined) {
      value = process.env[CONSTANT_CASE];
    } else {
      if(defaultValue === undefined) {
        value = question(`Please enter the value for ${variableName}: `);
      } else {
        value = question(`Please enter the value for ${variableName} [${defaultValue}]: `, {defaultInput: defaultValue});
      }
    }
    return this.cache.findValue[variableName] = value;
  }
}