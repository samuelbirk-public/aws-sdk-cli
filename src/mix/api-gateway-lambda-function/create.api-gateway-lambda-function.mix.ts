import {EcrCli} from '../../cli/ecr.cli';
import {ShellService} from '../../services/shell.service';
import {question} from 'readline-sync';
import {UtilsService} from '../../services/utils.service';
import {LambdaCli} from '../../cli/lambda.cli';
import {ApiGatewayCli} from '../../cli/apigateway.cli';
import {findWhere, last} from 'underscore';
import {LoggerService} from '../../services/logger.service';
import {UUID} from 'angular2-uuid';
export class CreateApiGatewayLambdaFunctionMix {
  args: any;
  command: any;
  cache: any;
  step: any;
  constructor(args: any, command: string = 'lambda-mix') {
    this.args = args;
    this.cache = this.args.cache || {};
    this.step = this.args.step || 1;
  }

  async run() {
    if(this.args.method === undefined || this.args.all) {
      this.args.methods = [
        'ensureDockerRepo',
        'buildAndPushDockerImage',
        'createLambdaFunction',
        'createApiGateway',
        'createIntegration',
        'createRoute',
        'addPermissionToLambda',
      ];
    }

    this.args.method = this.args.methods.shift();
    if(this.args.method !== undefined) {
      const result = await this.safeRun(this.args.method);
      if(result === 'error') {
        return;
      }
      new CreateApiGatewayLambdaFunctionMix({...this.args, step: this.step, cache: this.cache}).run();
    } else {
      if(this.args.debug) {
        LoggerService.debug(this.cache);
      }
      return this.cache;
    }
  }

  async safeRun(method: string) {
    this.step = LoggerService.step(this.step, `Running $ ${method}`);
    try {
      return (this as any)[method]();
    } catch (error) {
      LoggerService.error(error);
      return 'error'
    }
  }

  async getPermission() {
    try {
      const policies = await new LambdaCli({
        run: 'getPolicy',
        debug: this.args.debug,
        FunctionName: this.findValue('RepositoryName'),
      }).run();
      return policies?.Policy?.Statement[0]
    } catch(e) {
      return
    }
  }

  async addPermissionToLambda() {
    const permission = await this.getPermission();
    if(permission !== undefined) {
      return permission;
    }
    const apiGateway = await this.getApiGateway();
    return this.cache.addPermissionToLambda = this.cache.addPermissionToLambda  || await new LambdaCli({
      run: 'addPermission',
      debug: this.args.debug,
      StatementId: UUID.UUID(),
      Action: 'lambda:InvokeFunction',
      FunctionName: this.findValue('RepositoryName'),
      Principal: 'apigateway.amazonaws.com',
      SourceArn:  `arn:aws:execute-api:${this.findValue('AwsRegion')}:${this.findValue('AwsAccountId')}:${apiGateway.ApiId}/*/*/${this.findValue('RepositoryName')}`,
    }).run()
  }

  async getRoutes() {

    const apiGateway = await this.getApiGateway();
    try {
      const routes = await new ApiGatewayCli({
        run: 'getRoutes',
        debug: this.args.debug,
        ApiId: apiGateway.ApiId,
      }).run();
      return routes.Items;
    } catch (e) {
      return []
    }
  }


  async createRoute() {
    if(this.cache.createRoute !== undefined) {
      return this.cache.createRoute;
    }
    const apiGateway = await this.getApiGateway();
    const integration = await this.getIntegration();
    const routes = await this.getRoutes();
    const route = findWhere(routes, {RouteKey: `ANY /${this.findValue('RepositoryName')}`});
    if(route) {
      return this.cache.createRoute = route;
    }
    return this.cache.createRoute = this.cache.createRoute || await new ApiGatewayCli({
      run: 'createRoute',
      debug: this.args.debug,
      ApiId: apiGateway.ApiId,
      RouteKey: `ANY /${this.findValue('RepositoryName')}`,
      Target: `integrations/${integration.IntegrationId}`,
    }).run()
  }

  async getIntegration() {
    if( this.cache.getIntegration !== undefined) {
      return  this.cache.getIntegration;
    }
    const apiGateway = await this.getApiGateway();

    const integrations = await new ApiGatewayCli({
      run: 'getIntegrations',
      ApiId: apiGateway.ApiId
    }).run();

    return this.cache.getIntegration = last(integrations.Items);
  }

  async createIntegration() {
    if(this.cache.createIntegration !== undefined) {
      return this.cache.createIntegration;
    }
    const lambdaFunction = await this.getLambdaFunction();
    const apiGateway = await this.getApiGateway();
    const integration = await this.getIntegration();
    if(integration !== undefined) {
      return this.cache.createIntegration = integration;
    }
    return this.cache.createIntegration = await new ApiGatewayCli({
      run: 'createIntegration',
      debug: this.args.debug,
      ApiId: apiGateway.ApiId,
      TimeoutInMillis: '30000',
      IntegrationType: 'AWS_PROXY',
      PayloadFormatVersion: '2.0',
      IntegrationUri: lambdaFunction.Configuration.FunctionArn,
    }).run()
  }

  async createApiGateway(attempts = 0) {
    if(this.cache.createApiGateway !== undefined) {
      if(attempts > 10) {
        LoggerService.error('Failed to get APIGateway')
      }
      else if(this.getApiGateway() !== undefined) {
        return this.cache.createApiGateway;
      } else {
        const nextAttempt = attempts++;
        LoggerService.info(`Trying again in ${nextAttempt} seconds.`);
        setTimeout(() => this.createApiGateway(nextAttempt), nextAttempt * 1000);

      }
    }
    const lambdaFunction = await this.getLambdaFunction();
    this.cache.createApiGateway = await new ApiGatewayCli({
      run: 'createApi',
      debug: this.args.debug,
      Name: this.findValue('RepositoryName'),
      ProtocolType: 'HTTP',
      Target: lambdaFunction.Configuration.FunctionArn,
    }).run();

    if(this.getApiGateway() !== undefined) {
      return this.cache.createApiGateway;
    } else {
      this.createApiGateway(attempts++)
    }
  }


  async getApiGateway() {
    if(this.cache.getApiGateway !== undefined) {
      return this.cache.getApiGateway;
    }
    const apiGateways = await new ApiGatewayCli({
      run: 'getApis',
      debug: this.args.debug,
    }).run();

    return this.cache.getApiGateway = findWhere(apiGateways.Items,  { Name: this.findValue('RepositoryName')})
  }

  async getLambdaFunction() {
    return this.cache.getLambdaFunction = this.cache.getLambdaFunction  || await new LambdaCli({
      run: 'getFunction', FunctionName: this.findValue('RepositoryName'),
      debug: this.args.debug,
    }).run()
  }

  sleep(seconds = 0) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < (seconds * 1000));
  }

  async createLambdaFunction(attempts = 0) {
    if(this.cache.createLambdaFunction !== undefined) {
      const lambdaFunction = await this.getLambdaFunction();

      if(attempts > 10) {
        LoggerService.error('Failed to get APIGateway')
      }
      else if(lambdaFunction !== undefined) {
        return this.cache.createLambdaFunction = lambdaFunction;
      } else {
        const nextAttempt = attempts++;
        LoggerService.info(`Trying again in ${nextAttempt} seconds.`);
        this.sleep(nextAttempt);
        await this.createLambdaFunction(nextAttempt);
      }
    }
    this.cache.createLambdaFunction = await new LambdaCli({
      run: 'createFunction',
      debug: this.args.debug,
      FunctionName: this.findValue('RepositoryName'),
      Role: this.findValue('AWS_LAMBDA_ROLE_ARN'),
      Code: {
        ImageUri:`${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`,
      },
      PackageType: 'Image'
    }).run();
    const lambdaFunction = await this.getLambdaFunction();
    if(lambdaFunction !== undefined) {
      return this.cache.createLambdaFunction ;
    } else {
      this.createLambdaFunction(attempts++)
    }
  }


  imageTag() {
    return this.args.tag || 'latest'
  }


  buildAndPushDockerImage() {
    ShellService.command(`docker build -t ${this.findValue('RepositoryName')} .`, this.args.debug);
    ShellService.command(`docker tag ${this.findValue('RepositoryName')}:${this.imageTag()} ${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`, this.args.debug);
    ShellService.command(`docker push ${this.findValue('AwsAccountId')}.dkr.ecr.us-east-1.amazonaws.com/${this.findValue('RepositoryName')}:${this.imageTag()}`, this.args.debug);
  }

  async ensureDockerRepo() {
    await new EcrCli({run: 'createRepository', debug: this.args.debug, repositoryName: this.findValue('RepositoryName')}).run();

  }

  findValue(variableName: string, defaultValue?: string) {
    if(this.cache.findValue === undefined) {
      this.cache.findValue = {}
    }
    if(this.cache.findValue[variableName] !== undefined) {
      return this.cache.findValue[variableName];
    }
    const camelCase = UtilsService.titleToCamelCase(variableName);
    const CONSTANT_CASE = UtilsService.camelToUnderscore(camelCase).toUpperCase();
    let value;
    if(this.args[camelCase] !== undefined) {
      value = this.args[camelCase];
    } else if(process.env[CONSTANT_CASE] !== undefined) {
      value = process.env[CONSTANT_CASE];
    } else {
      if(defaultValue === undefined) {
        value = question(`Please enter the value for ${variableName}: `);
      } else {
        value = question(`Please enter the value for ${variableName} [${defaultValue}]: `, {defaultInput: defaultValue});
      }
    }
    return this.cache.findValue[variableName] = value;
  }


}