import {EcrCli} from '../../cli/ecr.cli';
import {ShellService} from '../../services/shell.service';
import {question} from 'readline-sync';
import {UtilsService} from '../../services/utils.service';
import {LambdaCli} from '../../cli/lambda.cli';
import {ApiGatewayCli} from '../../cli/apigateway.cli';
import {findWhere, last} from 'underscore';
import {LoggerService} from '../../services/logger.service';
import {UUID} from 'angular2-uuid';
export class DeleteApiGatewayLambdaFunctionMix {
  args: any;
  command: any;
  cache: any;
  step: any;
  constructor(args: any, command: string = 'lambda-mix') {
    this.args = args;
    this.cache = this.args.cache || {};
    this.step = this.args.step || 1;
  }

  async run() {
    if(this.args.method === undefined || this.args.all) {
      this.args.methods = [
        'removePermissionFromLambda',
        'deleteRoutes',
        'deleteIntegration',
        'deleteApiGateway',
        'deleteLambdaFunction',
        'deleteDockerRepo',
      ];
    }

    this.args.method = this.args.methods.shift();
    if(this.args.method !== undefined) {
      const result = await this.safeRun(this.args.method);
/*      if(result === 'error') {
        return;
      }*/
      new DeleteApiGatewayLambdaFunctionMix({...this.args, step: this.step, cache: this.cache}).run();
    } else {
      return this.cache;
    }
  }

  async safeRun(method: string) {
    this.step = LoggerService.step(this.step, `Running $ ${method}`);
    try {
      return (this as any)[method]();
    } catch (error) {
      LoggerService.error(error);
      return 'error'
    }
  }


  async getPermission() {
    try {
      const policies = await new LambdaCli({
        run: 'getPolicy',
        debug: this.args.debug,
        FunctionName: this.findValue('RepositoryName'),
      }).run();
      console.log(this.findValue('RepositoryName'));
      console.log(Object.keys(JSON.parse(policies.Policy)));
      return JSON.parse(policies?.Policy)?.Statement[0]
    } catch(e) {
      return
    }
  }

  async removePermissionFromLambda() {
    const permission = await this.getPermission();
    if(permission == undefined) {
      return;
    }
    return this.cache.addPermissionToLambda = this.cache.addPermissionToLambda  || await new LambdaCli({
      run: 'removePermission',
      debug: this.args.debug,
      StatementId: permission.Sid,
      FunctionName: this.findValue('RepositoryName'),
    }).run()
  }

  async getRoutes() {

    const apiGateway = await this.getApiGateway();
    try {
      const routes = await new ApiGatewayCli({
        run: 'getRoutes',
        debug: this.args.debug,
        ApiId: apiGateway.ApiId,
      }).run();
      return routes.Items;
    } catch (e) {
      return []
    }
  }

  async deleteRoutes() {
    const apiGateway = await this.getApiGateway();
    if(apiGateway === undefined) { return }
    const routes = await this.getRoutes();
    return this.cache.deleteRoutes = this.cache.deleteRoutes || await Promise.all(routes.map( async (route: any) => {

      return await new ApiGatewayCli({
        run: 'deleteRoute',
        debug: this.args.debug,
        ApiId: apiGateway.ApiId,
        RouteId: route.RouteId,
      }).run()
    }));
  }

  async getIntegrations() {
    if( this.cache.getIntegrations !== undefined) {
      return  this.cache.getIntegrations;
    }
    try {

      const apiGateway = await this.getApiGateway();
      if(apiGateway === undefined) { return }

      const integrations = await new ApiGatewayCli({
        run: 'getIntegrations',
        ApiId: apiGateway.ApiId
      }).run();

      return this.cache.getIntegrations = integrations?.Items || [];
    } catch (e) {
      return []
    }
  }

  async deleteIntegration() {
    const integrations = await this.getIntegrations();
    const apiGateway = await this.getApiGateway();
    if(apiGateway === undefined) { return }

    return this.cache.deleteIntegration = this.cache.deleteIntegration || await Promise.all(integrations.map(async (integration: any) => {
      return await new ApiGatewayCli({
        run: 'deleteIntegration',
        debug: this.args.debug,
        ApiId: apiGateway.ApiId,
        IntegrationId: integration.IntegrationId
      }).run()
    }));
  }

  async deleteApiGateway() {
    const apiGateway = await this.getApiGateway();
    if(apiGateway === undefined) { return }
    this.cache.deleteApiGateway = await new ApiGatewayCli({
      run: 'deleteApi',
      debug: this.args.debug,
      ApiId: apiGateway.ApiId,
    }).run();
  }

  async getApiGateway() {
    if(this.cache.getApiGateway !== undefined) {
      return this.cache.getApiGateway;
    }
    try {
      const apiGateways = await new ApiGatewayCli({
        run: 'getApis',
        debug: this.args.debug,
      }).run();

      return this.cache.getApiGateway = findWhere(apiGateways.Items,  { Name: this.findValue('RepositoryName')})
    } catch (e) {
      return;
    }

  }

  async getLambdaFunction() {

    return this.cache.getLambdaFunction = this.cache.getLambdaFunction  || await new LambdaCli({
      run: 'getFunction', FunctionName: this.findValue('RepositoryName'),
      debug: this.args.debug,
    }).run()
  }

  sleep(seconds = 0) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < (seconds * 1000));
  }

  async deleteLambdaFunction() {
    this.cache.deleteLambdaFunction = await new LambdaCli({
      run: 'deleteFunction',
      debug: this.args.debug,
      FunctionName: this.findValue('RepositoryName'),
    }).run();
  }

  async deleteDockerRepo() {
    await new EcrCli({
      run: 'deleteRepository',
      debug: this.args.debug,
      force: true,
      repositoryName: this.findValue('RepositoryName')
    }).run();

  }

  findValue(variableName: string, defaultValue?: string) {
    if(this.cache.findValue === undefined) {
      this.cache.findValue = {}
    }
    if(this.cache.findValue[variableName] !== undefined) {
      return this.cache.findValue[variableName];
    }
    const camelCase = UtilsService.titleToCamelCase(variableName);
    const CONSTANT_CASE = UtilsService.camelToUnderscore(camelCase).toUpperCase();
    console.log('variableName', variableName);
    console.log('camelCase', camelCase);
    console.log('CONSTANT_CASE', CONSTANT_CASE);
    let value;
    if(this.args[camelCase] !== undefined) {
      value = this.args[camelCase];
    } else if(process.env[CONSTANT_CASE] !== undefined) {
      value = process.env[CONSTANT_CASE];
    } else {
      if(defaultValue === undefined) {
        value = question(`Please enter the value for ${variableName}: `);
      } else {
        value = question(`Please enter the value for ${variableName} [${defaultValue}]: `, {defaultInput: defaultValue});
      }
    }
    return this.cache.findValue[variableName] = value;
  }


}