import {question} from 'readline-sync';
import {UtilsService} from '../../services/utils.service';
import {LambdaCli} from '../../cli/lambda.cli';
import {LoggerService} from '../../services/logger.service';
import {ShellService} from "../../services/shell.service";
import {S3Cli} from "../../cli/s3.cli";
import {exit} from "shelljs";
import {SqsCli} from "../../cli/sqs.cli";

export class UpdateEventSourceSqsMix {
  args: any;
  command: any;
  cache: any;
  step: any;
  constructor(args: any, command: string = 'purge-sqs-mix') {
    this.args = args;
    this.cache = this.args.cache || {};
    this.step = this.args.step || 1;
  }

  async run() {
    if(this.args.methods === undefined || this.args.all) {
      this.args.methods = [
        'updateEventSource',
      ];
    }

    const method = this.args.methods.shift();
    if(method !== undefined) {
      const result = await this.safeRun(method);
      if(result === 'error') {
        return;
      }
      await new UpdateEventSourceSqsMix({...this.args, step: this.step, cache: this.cache}).run();
    } else {
      if(this.args.debug) {
        LoggerService.debug(this.cache);
      }
      return this.cache;
    }
  }

  async updateEventSource() {
    const {EventSourceMappings} = await this.getEventSourceMappings();
    console.log('res', JSON.stringify(EventSourceMappings));
    return await Promise.all(EventSourceMappings.map(async (eventSource: string) => {
      return await this.updateEventSourceMapping(eventSource);
    }));
  }

  async updateEventSourceMapping(eventSource: any) {
    console.log('eventSource.EventSourceArn', eventSource)
    if(eventSource.EventSourceArn.endsWith('fifo')) {
      eventSource.BatchSize = 10
      eventSource.MaximumBatchingWindowInSeconds = 0
    } else {
      eventSource.BatchSize = 50
      eventSource.MaximumBatchingWindowInSeconds = 60
    }
    delete eventSource.EventSourceArn
    delete eventSource.FunctionArn
    delete eventSource.LastModified
    delete eventSource.State
    delete eventSource.StateTransitionReason
    return await new LambdaCli({
      run: 'updateEventSourceMapping',
      debug: this.args.debug,
      region: this.args.region,
      ...eventSource
    }).run()
  }

  async getEventSourceMappings() {
    return this.cache.getEventSourceMappings = this.cache.getEventSourceMappings ?? await new LambdaCli({
      run: 'listEventSourceMappings',
      debug: this.args.debug,
      region: this.args.region,
      FunctionName: this.findValue('RepositoryName')
    }).run()
  }

  async safeRun(method: string) {
    this.step = LoggerService.step(this.step, `Running $ ${method}`);
    try {
      return (this as any)[method]();
    } catch (error) {
      LoggerService.error(error);
      return 'error'
    }
  }

  findValue(variableName: string, defaultValue?: string) {
    if(this.cache.findValue === undefined) {
      this.cache.findValue = {}
    }
    if(this.cache.findValue[variableName] !== undefined) {
      return this.cache.findValue[variableName];
    }
    const camelCase = UtilsService.titleToCamelCase(variableName);
    const CONSTANT_CASE = UtilsService.camelToUnderscore(camelCase).toUpperCase();
    console.log('variableName', variableName);
    console.log('camelCase', camelCase);
    console.log('CONSTANT_CASE', CONSTANT_CASE);
    let value;
    if(this.args[camelCase] !== undefined) {
      value = this.args[camelCase];
    } else if(process.env[CONSTANT_CASE] !== undefined) {
      value = process.env[CONSTANT_CASE];
    } else {
      if(defaultValue === undefined) {
        value = question(`Please enter the value for ${variableName}: `);
      } else {
        value = question(`Please enter the value for ${variableName} [${defaultValue}]: `, {defaultInput: defaultValue});
      }
    }
    return this.cache.findValue[variableName] = value;
  }
}