import {question} from 'readline-sync';
import {UtilsService} from '../../services/utils.service';
import {LambdaCli} from '../../cli/lambda.cli';
import {LoggerService} from '../../services/logger.service';
import {ShellService} from "../../services/shell.service";
import {S3Cli} from "../../cli/s3.cli";
import {exit} from "shelljs";
import {SqsCli} from "../../cli/sqs.cli";

export class PurgeSqsMix {
  args: any;
  command: any;
  cache: any;
  step: any;
  constructor(args: any, command: string = 'purge-sqs-mix') {
    this.args = args;
    this.cache = this.args.cache || {};
    this.step = this.args.step || 1;
  }

  async run() {
    if(this.args.methods === undefined || this.args.all) {
      this.args.methods = [
        'purgeQueues',
      ];
    }

    const method = this.args.methods.shift();
    if(method !== undefined) {
      const result = await this.safeRun(method);
      if(result === 'error') {
        return;
      }
      await new PurgeSqsMix({...this.args, step: this.step, cache: this.cache}).run();
    } else {
      if(this.args.debug) {
        LoggerService.debug(this.cache);
      }
      return this.cache;
    }
  }

  async purgeQueues() {
    const {QueueUrls} = await this.getQueues();
    return await Promise.all(QueueUrls.map(async (url: string) => {
      await this.purgeQueue(url);
    }));
  }

  async purgeQueue(url: string) {
    return await new SqsCli({
      run: 'purgeQueue',
      debug: this.args.debug,
      region: this.args.region,
      QueueUrl: url
    }).run()
  }

  async getQueues() {
    return this.cache.getQueues = this.cache.getQueues ?? await new SqsCli({
      run: 'listQueues',
      debug: this.args.debug,
      region: this.args.region,
      QueueNamePrefix: this.args.prefix
    }).run()
  }

  async safeRun(method: string) {
    this.step = LoggerService.step(this.step, `Running $ ${method}`);
    try {
      return (this as any)[method]();
    } catch (error) {
      LoggerService.error(error);
      return 'error'
    }
  }

  findValue(variableName: string, defaultValue?: string) {
    if(this.cache.findValue === undefined) {
      this.cache.findValue = {}
    }
    if(this.cache.findValue[variableName] !== undefined) {
      return this.cache.findValue[variableName];
    }
    const camelCase = UtilsService.titleToCamelCase(variableName);
    const CONSTANT_CASE = UtilsService.camelToUnderscore(camelCase).toUpperCase();
    console.log('variableName', variableName);
    console.log('camelCase', camelCase);
    console.log('CONSTANT_CASE', CONSTANT_CASE);
    let value;
    if(this.args[camelCase] !== undefined) {
      value = this.args[camelCase];
    } else if(process.env[CONSTANT_CASE] !== undefined) {
      value = process.env[CONSTANT_CASE];
    } else {
      if(defaultValue === undefined) {
        value = question(`Please enter the value for ${variableName}: `);
      } else {
        value = question(`Please enter the value for ${variableName} [${defaultValue}]: `, {defaultInput: defaultValue});
      }
    }
    return this.cache.findValue[variableName] = value;
  }
}