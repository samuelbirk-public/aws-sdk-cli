import {LoggerService} from "../services/logger.service";
import {isUndefined, omit, union, uniq} from 'underscore';
import {question} from "readline-sync";
import {PreCheckNavigation} from "./pre-check.navigation";
import {UtilsService} from '../services/utils.service';
import {RoutesNavigation} from "./routes.navigation";

const clear = require('clear');
const figlet = require('figlet');
const chalk = require('chalk');

export class CliNavigation {
  possibleCommands = [
    'help',
  ];
  cliName: string = 'cli';
  constructor(args: any = {}) {
    this.cliName = 'aws-sdk-cli2';
    if(!args.hideHeader && [undefined, null].includes(args._[0])) {
      this.displayIntro();
    }
    this.run(args);
  }

  displayIntro() {
    clear();
    console.log(
        chalk.yellow(
            figlet.textSync(this.cliName, { horizontalLayout: 'full' })
        )
    );
  }

  classExistsForCommand(name: string) {
    return this.classFromCommand(name) !== undefined;
  }

  classFromCommand(command: string) {
    const className = UtilsService.snakeToTitleCase(command);
    return this.classes()[className];
  }

  classes(): any {
    return RoutesNavigation.routes();
  }

  run(args: any) {
    const command = args._[0];
    if(args.h || command ==='help' || (!this.possibleCommands.includes(command)) && !this.classExistsForCommand(command)) {
      if(![undefined, null, ''].includes(command)) {
        LoggerService.error(`'${command}' is not an option.`);
      }
      LoggerService.grey('Please choose one of the following:');
      const commandsFromClasses = Object.keys(this.classes()).map((klass: string) => {
        return UtilsService.camelToSnakeCase(klass.toString());
      });
      let allPossibleCommands = union(this.possibleCommands, commandsFromClasses)
      allPossibleCommands.map((cmd: string) => {
        LoggerService.grey(`${this.cliName} ${cmd}`);
      });

      const suggestions = allPossibleCommands.filter((cmd: string) => {
        if(cmd === undefined || command === undefined) {
          return false;
        }
        return cmd.includes(command) || command.includes(cmd);
      });
      if(suggestions.length > 0) {
        LoggerService.white('');
        LoggerService.white('Did you mean to do one of the following:');
        uniq(suggestions).map((cmd: string, index: number) => {
          LoggerService.white(`[${index + 1}] ${this.cliName} ${cmd}`);
        });
        LoggerService.white('');

        const rerunCommandString = question('Select a number or press any key to exit: ');
        const rerunCommandIndex = Number(rerunCommandString) - 1;
        if(Number(rerunCommandIndex) >= 0) {
          if(suggestions[rerunCommandIndex] == undefined) {
            LoggerService.warn(`${rerunCommandString} is not an option.`)
          } else {
            args._[0] = suggestions[rerunCommandIndex];
            new CliNavigation(args);
          }
        }
        LoggerService.white('');
      }
      return;
    }
    LoggerService.grey(`Running $ ${this.cliName} ${command}`);
    if ([undefined, null, ''].includes(command)) {
      LoggerService.info('You have to choose a command.');
      LoggerService.warn(`example: '${this.cliName} help'`);
    } else {
      this.runCommand(command, args)
    }
  }

  async runCommand(command: string, args: any) {
    let passThroughArgs = {...args};
    delete passThroughArgs["_"];
    delete passThroughArgs["$0"];
    passThroughArgs.debug = passThroughArgs.debug || passThroughArgs.verbose;
    passThroughArgs.verbose = passThroughArgs.verbose || passThroughArgs.debug;
    passThroughArgs = omit(passThroughArgs, isUndefined)
    const preCheckSatisfied = new PreCheckNavigation(passThroughArgs, command).satisfied();
    if(!preCheckSatisfied) {
      return;
    }
    const klass = this.classFromCommand(command);
    const instance = new klass(passThroughArgs, command);
    const asyncFunc: boolean = instance.run.toString().includes('__awaiter(')
    if(asyncFunc) {
      await instance.run();
    } else {
      instance.run();
    }
  }

}
