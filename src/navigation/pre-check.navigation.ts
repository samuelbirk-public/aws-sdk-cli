import {UtilsService} from "../services/utils.service";
import {AwsCredentials} from "../credentials/aws.credentials";

export class PreCheckNavigation {
  args: any;
  command: string;
  constructor(args: any, command: string) {
    this.args = args;
    this.command = command;
  }

  satisfied(): any {
    return this.ensureCredentials();
  }

  ensureCredentials() {
    return new AwsCredentials(this.args, this.command).run() || false;
  }



}