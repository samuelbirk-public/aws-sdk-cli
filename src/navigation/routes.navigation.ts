import {LambdaCli} from "../cli/lambda.cli";
import {CreateApiGatewayLambdaFunctionMix} from "../mix/api-gateway-lambda-function/create.api-gateway-lambda-function.mix";
import {UpdateApiGatewayLambdaFunctionMix} from "../mix/api-gateway-lambda-function/update.api-gateway-lambda-function.mix";
import {DeleteApiGatewayLambdaFunctionMix} from "../mix/api-gateway-lambda-function/delete.api-gateway-lambda-function.mix";
import {CreateDynamodbLambdaFunctionMix} from "../mix/dynamodb-lambda-function/create.dynamodb-lambda-function.mix";
import {UpdateDynamodbLambdaFunctionMix} from "../mix/dynamodb-lambda-function/update.dynamodb-lambda-function.mix";
import {CreateSqsLambdaFunctionMix} from "../mix/sqs-lambda-function/create.sqs-lambda-function.mix";
import {UpdateSqsLambdaFunctionMix} from "../mix/sqs-lambda-function/update.sqs-lambda-function.mix";
import {UpdateLambdaFunctionMix} from "../mix/lambda-function/update.lambda-function.mix";
import {PurgeSqsMix} from "../mix/sqs/purge.sqs.mix";
import {UpdateEventSourceSqsMix} from "../mix/sqs/update-event-source.sqs.mix";

export class RoutesNavigation {
  static routes(): any {
    return {
      LambdaCli,
      CreateApiGatewayLambdaFunctionMix,
      UpdateApiGatewayLambdaFunctionMix,
      DeleteApiGatewayLambdaFunctionMix,
      CreateDynamodbLambdaFunctionMix,
      UpdateDynamodbLambdaFunctionMix,
      CreateSqsLambdaFunctionMix,
      UpdateSqsLambdaFunctionMix,
      UpdateLambdaFunctionMix,
      PurgeSqsMix,
      UpdateEventSourceSqsMix
    }
  }
}
