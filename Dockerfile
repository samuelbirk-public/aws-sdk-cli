FROM registry.gitlab.com/samuelbirk-public/awsv2
RUN apt-get -y update

WORKDIR /app
ADD . /app
RUN npm install
RUN npm install -g typescript ts-node

RUN tsc
CMD /bin/bash
