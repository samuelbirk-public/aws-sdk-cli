# cfn-cli
Cloudformation made easy
## Setup
- ```git clone repo```
- You will probably want to add the following to your .zshrc or .bash_profile
```bash
alias cfn-cli="ts-node $HOME/path/to/code/cfn-cli/src/index.ts"
```
- npm install -g  ts-node tsc typescript
- Download docker from https://www.docker.com/products/docker-desktop
- ``cfn-cli new``
- ``cfn-cli help``